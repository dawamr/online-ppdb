<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Kelola Kejuruan</h1>

    <div class="row">
        <di class="col-md-12">
            <div class="card">
                <div class="card-header">List Kejuruan</div>
                <div class="card-body">
                    <div class="buttons">
                        <a href="<?php echo base_url() . 'kelola_kejuruan/create' ?>" class="btn btn-primary btn-sm">
                            <i class="fas fa-plus"></i>
                            Tambah Baru
                        </a>
                    </div>
                    <div class="table-responsive mt-3">
                        <table id="list-program-keahlian" class="table table-hover nowrap">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Kode Program</th>
                                    <th>Nama Program Keahlian</th>
                                    <th>Kuota Siswa</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </di>
    </div>

</div>
<!-- /.container-fluid -->