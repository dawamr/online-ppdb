<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Kelola Program Keahlian</h1>

    <div class="row">
        <di class="col-md-12">
            <div class="card">
                <div class="card-header">Tambah Program Keahlian</div>
                <div class="card-body">
                    <div class="form">
                        <form action="" method="post">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Kode Program</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" placeholder="Kode Program">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Nama Program</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" placeholder="Nama Program">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Kuota Siswa</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" placeholder="Kuota Siswa">
                                </div>
                                <p class="col-sm-2 col-form-label">Orang</p>
                            </div>
                            <div class="form-group ml-1 row">
                                <div class="buttons offset-2">
                                    <button class="btn btn-primary btn-sm" type="submit">Simpan</button>
                                    <a href="<?php echo base_url() . 'kelola_kejuruan'; ?>" class="btn btn-sm btn-warning">Cancel</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </di>
    </div>

</div>
<!-- /.container-fluid -->