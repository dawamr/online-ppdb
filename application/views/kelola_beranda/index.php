<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Kelola Beranda</h1>

    <div class="row">
        <di class="col-md-12">
            <div class="card mb-4">
                <div class="card-body">
                    <div id="summernote-inline">
                        <h6 class="m-0 font-weight-bold">Visi SMK Widya Praja Ungaran</h6>
                        <p>Visi dari SMK Widya Praja Ungaran yaitu belajar dan berlatih untuk bekerja dan mandiri melalui Iptek dan Imtaq yang mampu berkompetensi di tingkat nasional dan internasional.</p>
                        <br>
                        <h6 class="m-0 font-weight-bold">Misi SMK Widya Praja Ungaran</h6>
                        <ol>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ol>
                        <br>
                        <h6 class="m-0 font-weight-bold">Program Keahlian SMK Widya Praja Ungaran</h6>
                        <ol>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ol>
                    </div> <!-- end summernote-editor-->   
                </div>
            </div>
        </di>
    </div>

</div>
<!-- /.container-fluid -->