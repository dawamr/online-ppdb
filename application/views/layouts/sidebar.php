<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">{ Role }</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item">
        <a class="nav-link text-center">
            <strong>{ Nama }</strong></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Navigasi
    </div>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link bt-sidebar" href="<?php echo base_url() . 'kelola-pengguna' ?>">
            <i class="fas fa-fw fa-cog"></i>
            <span>Kelola Pengguna</span>
        </a>
    </li>
    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link bt-sidebar" href="<?php echo base_url() . 'kelola_beranda' ?>">
            <i class="fas fa-fw fa-cog"></i>
            <span>Kelola Beranda</span>
        </a>
    </li>
    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link bt-sidebar">
            <i class="fas fa-fw fa-cog"></i>
            <span>Kelola Prosedur</span>
        </a>
    </li>
    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link bt-sidebar">
            <i class="fas fa-fw fa-cog"></i>
            <span>Kelola Pengumuman</span>
        </a>
    </li>
    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link bt-sidebar" href="<?php echo base_url() . 'kelola_kejuruan' ?>">
            <i class="fas fa-fw fa-cog"></i>
            <span>Kelola Kejuruan</span>
        </a>
    </li>
    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link bt-sidebar" href="<?php echo base_url() . 'kelola_pendaftaran' ?>">
            <i class="fas fa-fw fa-cog"></i>
            <span>Kelola Pendaftaran</span>
        </a>
    </li>


    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
<!-- End of Sidebar -->