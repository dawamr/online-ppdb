<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Kelola Pendaftaran</h1>

    <div class="row">
        <di class="col-md-12">
            <div class="card">
                <div class="card-header">List Pendaftar</div>
                <div class="card-body">
                    <div class="table-responsive mt-3">
                        <table id="list-pendaftar" class="table table-hover nowrap">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>No Pendaftaran</th>
                                    <th>Nama Siswa</th>
                                    <th>Asal Sekolah</th>
                                    <th>Jurusan 1</th>
                                    <th>Jurusan 2</th>
                                    <th>Skor Akhir</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </di>
    </div>

</div>
<!-- /.container-fluid -->