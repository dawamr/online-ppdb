<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-4 text-gray-800">Kelola Pengguna</h1>

<div class="row">
    <di class="col-md-12">
        <div class="card">
            <div class="card-header">List Pengguna</div>
            <div class="card-body">
                <div class="buttons">
                    <a href="<?php echo base_url() . 'kelola-pengguna/tambah' ?>" class="btn btn-primary btn-sm">
                        <i class="fas fa-plus"></i>
                        Tambah Baru
                    </a>
                </div>
                <div class="table-responsive mt-3">
                    <table id="list-pengguna" class="table table-hover nowrap">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nama Pengguna</th>
                                <th>Nama Pegawai</th>
                                <th>Posisi</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </di>
</div>

</div>
<!-- /.container-fluid -->