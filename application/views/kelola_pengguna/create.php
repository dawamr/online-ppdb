<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-4 text-gray-800">Kelola Pengguna</h1>

<div class="row">
    <di class="col-md-12">
        <div class="card">
            <div class="card-header">Tambah Pengguna</div>
            <div class="card-body">
                <div class="form">
                    <form action="" method="post"> 
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Nama Pengguna</label>
                            <div class="col-sm-10">
                            <input type="text" class="form-control" placeholder="Nama Pengguna">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Nama Pegawai</label>
                            <div class="col-sm-10">
                            <input type="text" class="form-control" placeholder="Nama Pengguna">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Pilih Jabatan</label>
                            <div class="col-sm-10">
                                <select name="" id="" class="form-control">
                                    <option value="">Panitia Pendaftaran</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Kata Sandi</label>
                            <div class="col-sm-10">
                            <input type="password" class="form-control" placeholder="Kata Sandi">
                            </div>
                        </div>
                        <div class="form-group ml-1 row">
                            <div class="buttons offset-2">
                                <button class="btn btn-primary btn-sm" type="submit">Simpan</button>
                                <a href="<?php echo base_url() . 'kelola-pengguna'; ?>" class="btn btn-sm btn-warning">Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </di>
</div>

</div>
<!-- /.container-fluid -->