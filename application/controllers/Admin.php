<?php

class Admin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
    }

    public function index()
    {
        $data['title'] = 'Dashboard';
        $this->load->view('tema/header', $data);
        $this->load->view('tema/sidebar');
        $this->load->view('tema/index');
        $this->load->view('tema/footer');
    }
}
