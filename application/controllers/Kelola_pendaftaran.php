<?php

class Kelola_pendaftaran extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
    }

    public function index()
    {
        $data = [];
        $data['style'] = $this->load->view('kelola_pendaftaran/style', $data, true);
        $data['script'] = $this->load->view('kelola_pendaftaran/script', $data, true);
        $data['content'] = $this->load->view('kelola_pendaftaran/index', $data, true);
        $this->load->view('layouts/header', $data);
    }
}
