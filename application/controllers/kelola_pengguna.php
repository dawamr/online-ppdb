<?php

class kelola_pengguna extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
    }

    public function index()
    {
        $data = [];
        $data['style'] = $this->load->view('kelola_pengguna/style', $data, true);
        $data['script'] = $this->load->view('kelola_pengguna/script', $data, true);
        $data['content'] = $this->load->view('kelola_pengguna/index', $data, true);
        $this->load->view('layouts/header', $data);
    }

    public function create(){
        $data = [];
        $data['style'] = $this->load->view('kelola_pengguna/style', $data, true);
        $data['script'] = $this->load->view('kelola_pengguna/script', $data, true);
        $data['content'] = $this->load->view('kelola_pengguna/create', $data, true);
        $this->load->view('layouts/header', $data);
    }
}
