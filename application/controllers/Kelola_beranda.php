<?php

class Kelola_beranda extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
    }

    public function index()
    {
        $data = [];
        $data['style'] = $this->load->view('kelola_beranda/style', $data, true);
        $data['script'] = $this->load->view('kelola_beranda/script', $data, true);
        $data['content'] = $this->load->view('kelola_beranda/index', $data, true);
        $this->load->view('layouts/header', $data);
    }
}
