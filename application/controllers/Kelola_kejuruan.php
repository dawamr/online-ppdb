<?php

class Kelola_kejuruan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
    }

    public function index()
    {
        $data = [];
        $data['style'] = $this->load->view('kelola_kejuruan/style', $data, true);
        $data['script'] = $this->load->view('kelola_kejuruan/script', $data, true);
        $data['content'] = $this->load->view('kelola_kejuruan/index', $data, true);
        $this->load->view('layouts/header', $data);
    }

    public function create()
    {
        $data = [];
        $data['style'] = $this->load->view('kelola_kejuruan/style', $data, true);
        $data['script'] = $this->load->view('kelola_kejuruan/script', $data, true);
        $data['content'] = $this->load->view('kelola_kejuruan/create', $data, true);
        $this->load->view('layouts/header', $data);
    }
}
