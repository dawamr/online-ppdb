-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 12 Feb 2020 pada 03.48
-- Versi Server: 10.1.9-MariaDB
-- PHP Version: 7.0.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `online-ppdb`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_bayar_du`
--

CREATE TABLE `t_bayar_du` (
  `kode_bayar` int(11) NOT NULL,
  `tgl_bayar` date NOT NULL,
  `no_pendaftaran` varchar(500) NOT NULL,
  `kode_dana` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_beranda`
--

CREATE TABLE `t_beranda` (
  `id` int(11) NOT NULL,
  `judul_konten_beranda` varchar(225) NOT NULL,
  `isi_konten_beranda` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_dana_du`
--

CREATE TABLE `t_dana_du` (
  `kode_dana` int(11) NOT NULL,
  `nama_dana` varchar(225) NOT NULL,
  `jumlah_dana` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_jurusan`
--

CREATE TABLE `t_jurusan` (
  `id` int(11) NOT NULL,
  `kode_jurusan` varchar(225) NOT NULL,
  `nama_jurusan` varchar(225) NOT NULL,
  `kuota` int(11) NOT NULL,
  `bobot` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_pedaftaran`
--

CREATE TABLE `t_pedaftaran` (
  `id` int(11) NOT NULL,
  `no_pendaftaran` varchar(500) NOT NULL,
  `nama_pendaftar` varchar(225) NOT NULL,
  `jenis_kelamin` text NOT NULL,
  `smp_mts` varchar(225) NOT NULL,
  `no_ijasah` varchar(225) NOT NULL,
  `nisn` int(11) NOT NULL,
  `agama` int(11) NOT NULL,
  `alamat_rumah` varchar(225) NOT NULL,
  `ortu_wali` varchar(225) NOT NULL,
  `pilihan_1` int(11) NOT NULL,
  `pilihan_2` int(11) NOT NULL,
  `no_telp` varchar(13) NOT NULL,
  `nilai_un` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_prosedur`
--

CREATE TABLE `t_prosedur` (
  `id` int(11) NOT NULL,
  `judul_konten_prosedur` varchar(225) NOT NULL,
  `isi_konten_prosedur` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_user`
--

CREATE TABLE `t_user` (
  `id` int(11) NOT NULL,
  `username` varchar(225) NOT NULL,
  `nama_pegawai` varchar(225) NOT NULL,
  `posisi` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_wawancara`
--

CREATE TABLE `t_wawancara` (
  `no_ujian` int(11) NOT NULL,
  `no_pendaftaran` varchar(500) NOT NULL,
  `nilai_un` int(11) NOT NULL,
  `nilai_ujian_wawancara` int(11) NOT NULL,
  `nilai_akhir` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_bayar_du`
--
ALTER TABLE `t_bayar_du`
  ADD PRIMARY KEY (`kode_bayar`);

--
-- Indexes for table `t_beranda`
--
ALTER TABLE `t_beranda`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_dana_du`
--
ALTER TABLE `t_dana_du`
  ADD PRIMARY KEY (`kode_dana`);

--
-- Indexes for table `t_jurusan`
--
ALTER TABLE `t_jurusan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_pedaftaran`
--
ALTER TABLE `t_pedaftaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_prosedur`
--
ALTER TABLE `t_prosedur`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_user`
--
ALTER TABLE `t_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_wawancara`
--
ALTER TABLE `t_wawancara`
  ADD PRIMARY KEY (`no_ujian`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_bayar_du`
--
ALTER TABLE `t_bayar_du`
  MODIFY `kode_bayar` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_beranda`
--
ALTER TABLE `t_beranda`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_jurusan`
--
ALTER TABLE `t_jurusan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_pedaftaran`
--
ALTER TABLE `t_pedaftaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_prosedur`
--
ALTER TABLE `t_prosedur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_user`
--
ALTER TABLE `t_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_wawancara`
--
ALTER TABLE `t_wawancara`
  MODIFY `no_ujian` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
